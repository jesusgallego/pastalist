package es.ua.eps.pastalist;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

public class DetailActivity extends AppCompatActivity {

    public static String EXTRA_PASTA = "EXTRA_PASTA";

    private Pasta pasta;

    private ImageView imageView;
    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        // Back button
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        pasta = (Pasta) getIntent().getSerializableExtra(EXTRA_PASTA);

        imageView = (ImageView) findViewById(R.id.detail_image);
        textView = (TextView) findViewById(R.id.detail_name);

        if (pasta != null) {
            new PastaLoader().execute(pasta, imageView);
            textView.setText(pasta.getName());
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}
