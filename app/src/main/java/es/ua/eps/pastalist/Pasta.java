package es.ua.eps.pastalist;

import android.graphics.Bitmap;

import java.io.Serializable;

public class Pasta implements Serializable
{
    private String mUrl;
    private String mName;
    private Bitmap mBitmap;

    Pasta( String _url, String _name ) {
        mUrl = _url;
        mName = _name;
        mBitmap = null;
    }

    public String getUrl() { return mUrl; }
    public void setUrl( String _url ) { mUrl = _url; }

    public String getName() { return mName; }
    public void setName( String _name ) { mName = _name; }

    public Bitmap getBitmap() { return mBitmap; }
    public void setBitmap( Bitmap _bitmap ) { mBitmap = _bitmap; }
}
