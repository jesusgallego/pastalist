package es.ua.eps.pastalist;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PastaAdapter extends BaseAdapter 
{
    private List<Pasta> mList;
    private Context mContext;
    Map<Pasta, PastaLoader> mImagenesCargando;

    public PastaAdapter(Context context, List<Pasta> objects)
    {
        mContext = context;
        mList = objects;
        mImagenesCargando = new HashMap<>();
    }

    @Override
    public int getCount()
    {
        return mList.size();
    }

    @Override
    public Object getItem(int position)
    {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position)
    {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        if(convertView == null) {
            LayoutInflater li = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = li.inflate(R.layout.list_item, null);
        }

        TextView tvTexto = (TextView) convertView.findViewById(R.id.textView);
        ImageView ivIcono = (ImageView) convertView.findViewById(R.id.imageView);

        Pasta pasta = mList.get(position);
        tvTexto.setText(pasta.getName());

        if (pasta.getBitmap() != null) {
            ivIcono.setImageBitmap(pasta.getBitmap());
        } else {
            if(mImagenesCargando.get(pasta)==null) {

                ivIcono.setImageResource(R.mipmap.ic_launcher);

                PastaLoader task = new PastaLoader();
                mImagenesCargando.put(pasta, task);
                task.execute(pasta, ivIcono);
            }
        }

        return convertView;
    }
}
